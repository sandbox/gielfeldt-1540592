<?php
/**
 * @file
 *
 * Pages for taxonomy mpath settings.
 */

/**
 * Form build for the settings form
 *
 * @ingroup forms
 */
function taxonomy_mpath_settings_form() {
  $form = array();

  $form['taxonomy_mpath_static_caching'] = array(
    '#title' => t('Use static caching'),
    '#description' => t('Use static caching for taxoomy_get_tree(). If experiencing memory exhausts, try disabling this.'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('taxonomy_mpath_static_caching', TAXONOMY_MPATH_STATIC_CACHING),
  );

  $form = system_settings_form($form);
  return $form;
}

