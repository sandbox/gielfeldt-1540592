<?php
/**
 * @file
 *
 * This file contains the core override functions
 */

/**
 * Get parents for a set of paths.
 *
 * @param array $paths
 *   Array of path (strings)
 * @return array
 *   Array of object containing info for all parents
 */
function _taxonomy_mpath_get_parents($paths) {
  $lookup = array();
  foreach ($paths as $path) {
    preg_match('/^(\d+\/\d+\/)(.*)$/', $path, $matches);
    array_shift($matches);
    list($base, $path) = $matches;
    $lookup[] = $base;
    foreach (explode(TAXONOMY_MPATH_SEPARATOR, $path) as $part) {
      if (empty($part)) {
        continue;
      }
      $base .= $part . TAXONOMY_MPATH_SEPARATOR;
      $lookup[] = $base;
    }
  }
  $query = db_select('taxonomy_mpath', 'm')
             ->fields('m', array('pid', 'path', 'distance'));
  $or = db_or();

  $lookup = array_unique($lookup);
  $lookup = array_diff($lookup, $paths);
  if (empty($lookup)) {
    return array();
  }
  foreach ($lookup as $path) {
    $or->condition('m.path', $path);
  }

  return $query->condition($or)
                  ->execute()
                  ->fetchAllAssoc('pid', PDO::FETCH_OBJ);
}

/**
 * Reimplementation of taxonomy_get_tree().
 * Limit db fetch to only specified parent AND use index for sorting.
 * @see taxonomy_get_tree()
 */
function taxonomy_mpath_taxonomy_get_tree($vid, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  static $cache = array();
  static $pids = array();
  static $parents = array();
  static $offsets = array();
  $locate = array();

  // Find pids for $parent
  if (!isset($pids[$vid][$parent])) {
    $pids[$vid][$parent] = db_select('taxonomy_mpath', 'm')
                             ->fields('m', array('pid', 'path', 'distance'))
                             ->condition('m.tid', $parent)
                             ->condition('m.vid', $vid)
                             ->execute()
                             ->fetchAllAssoc('pid', PDO::FETCH_OBJ);
  }

  // No path found for $parent ?
  if (empty($pids[$vid][$parent])) {
    return array();
  }

  if (variable_get('taxonomy_mpath_static_caching', TAXONOMY_MPATH_STATIC_CACHING)) {
    if (!isset($cache[$vid])) {
      $cache[$vid] = array();
    }

    // Find parent path for $parent
    if (!isset($parents[$vid][$parent])) {
      $parents[$vid][$parent] = array();
      foreach ($pids[$vid][$parent] as $pid) {
        $paths[] = $pid->path;
      }
      $parents[$vid][$parent] = _taxonomy_mpath_get_parents($paths);
    }

    // Se if we have any parents in our cache
    $locate = array_intersect(array_keys($parents[$vid][$parent]), array_keys($cache[$vid]));
  }
  
  // Choose a path for base
  $pid = reset($pids[$vid][$parent]);

  // If we are extracting a subtree from a previous cache, use that cache
  if ($locate) {
    $cache_pid = reset($locate);
    // If full cache, just return it
    if ($parents[$vid][$parent][$cache_pid]->distance == $pid->distance) {
      return $cache[$vid][$cache_pid];
    }

    // Locate subtree
    // @todo Can we just use array_slice() here, if we know the number of children?
    $tree = array();
    $depth = FALSE;
    $idx = $offsets[$vid][$cache_pid][$pid->pid];
    for ($i = $idx; $i < count($cache[$vid][$cache_pid]); $i++) {
      $term = $cache[$vid][$cache_pid][$i];
      if ($depth === FALSE) {
        if ($term->tid == $parent) {
          $depth = $term->depth + 1;
          continue;
        }
      }
      elseif ($depth > $term->depth) {
        break;
      }
      else {
        $term->depth -= $depth;
        $tree[] = $term;
      }
    }
    return $tree;
  }
  else {
    // No cache, let's fetch from db
    $query = db_select('taxonomy_mpath', 'm');
    $query->join('taxonomy_term_data', 'd', 'd.tid = m.tid');
    $query->join('taxonomy_term_hierarchy', 'h', 'h.tid = m.tid');

    $result = $query
      ->addTag('translatable')
      ->addTag('term_access')
      ->fields('m', array('pid', 'path', 'distance'))
      ->fields('d')
      ->fields('h', array('parent'))
      ->condition('m.path', db_like($pid->path) . '%', 'LIKE')
      ->orderBy('m.path');

    $result = $result->execute();

    $tree = array();
    $tids = array();
    $idx = 0;
    foreach ($result as $term) {
      // Don't include ourselves. We do this in code, because doing this in the
      // query might trigger a bad index choice by the database.
      if ($term->pid == $pid->pid) {
        continue;
      }

      if (!isset($tree[$term->pid])) {
        // New term in tree
        $term->depth = $term->distance - $pid->distance - 1;
        $term->parents = array($term->parent);
        unset($term->distance);
        unset($term->parent);
        $tree[$term->pid] = $term;
        $offsets[$vid][$pid->pid][$term->pid] = $idx++;
        unset($term->pid);
        $tids[] = $term->tid;
      }
      else {
        // Existing term
        $tree[$term->pid]->parents[] = $term->parent;
      }
    }
  }

  // Load full entities, if necessary. The entity controller statically
  // caches the results.
  if ($load_entities) {
    $term_entities = taxonomy_term_load_multiple($tids);
  }

  // Original taxonomy_get_tree() has an incremental index. Let's do this as well in a memory efficient way.
  $clean_tree = array();
  foreach ($tree as $idx => $term) {
    $clean_tree[] = $load_entities ? $term_entities[$term->tid] : $term;
    unset($tree[$idx]);
  }

  if (variable_get('taxonomy_mpath_static_caching', TAXONOMY_MPATH_STATIC_CACHING)) {
    $cache[$vid][$pid->pid] = $clean_tree;
    foreach ($pids[$vid][$parent] as $p) {
      if ($p->pid != $pid->pid) {
        $cache[$vid][$p->pid] = &$cache[$vid][$pid->pid];
        $offsets[$vid][$p->pid] = &$offsets[$vid][$pid->pid];
      }
    }
  }

  return $clean_tree;
}

