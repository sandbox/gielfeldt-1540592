<?php
/**
 * @file
 *
 * Contains the rebuild functionality of materialized paths
 * for Drupal taxonomies.
 */

/**
 * Helper function for getting batch for rebuilding.
 *
 * @param integer $vid
 *   Vocabulary ID to rebuild
 * @return
 *   Batch definition
 */
function taxonomy_mpath_batch_get_build($vid) {
  $batch = array(
    'operations' => array(
      array('taxonomy_mpath_batch_build', array($vid)),
    ),
    'finished' => 'taxonomy_mpath_batch_finished',
    'title' => t('Building materialized paths'),
    'init_message' => t('Building materialized paths'),
    'file' => drupal_get_path('module', 'taxonomy_mpath') . '/taxonomy_mpath.rebuild.inc',
  );
  return $batch;
}

/**
 * Batch finished
 */
function taxonomy_mpath_batch_finished($success, $results, $operations) {
  if ($success) {
    // Here we do something meaningful with the results.
    $message = theme('item_list', array('items' => $results));
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', array('%error_operation' => $error_operation[0], '@arguments' => print_r($error_operation[1], TRUE)));
  }
  drupal_set_message($message);
}

/**
 * Traverse entire tree and build left + right values
 *
 * @param integer $vid
 *   Vocabulary ID
 * @param mixed &$context
 *   Array reference to Drupal batch context
 */
function taxonomy_mpath_batch_build($vid, &$context) {
  // First time ... initialize variables
  if (!isset($context['sandbox']['depth'])) {
    $context['sandbox']['depth'] = 0;
    $context['sandbox']['terms'] = 0;
    $context['sandbox']['time'] = microtime(TRUE);
    db_delete('taxonomy_mpath')
      ->condition('path', db_like($vid . TAXONOMY_MPATH_SEPARATOR . '0' . TAXONOMY_MPATH_SEPARATOR) . '%', 'LIKE')
      ->execute();
    db_insert('taxonomy_mpath')
      ->fields(array(
        'vid' => $vid,
        'tid' => 0,
        'parent' => 0,
        'distance' => 0,
        'path' => $vid . TAXONOMY_MPATH_SEPARATOR . '0' . TAXONOMY_MPATH_SEPARATOR,
      ))
      ->execute();
  }

  // Setup local variables for better code readability
  $depth = &$context['sandbox']['depth'];
  $terms = &$context['sandbox']['terms'];

  $result = db_query("
    INSERT INTO {taxonomy_mpath} (vid, tid, parent, distance, path)
    SELECT d.vid, h.tid, h.parent, m.distance + 1,
    " . _taxonomy_mpath_db_concat('m.path', '(d.weight + 1500)', ':join', 'd.name', ':join', 'h.tid', ':separator') . " AS path
    FROM {taxonomy_term_hierarchy} h
    INNER JOIN {taxonomy_term_data} d ON d.tid = h.tid
    INNER JOIN {taxonomy_mpath} m ON m.tid = h.parent
    WHERE m.distance = :depth
    AND d.vid = :vid
    AND m.vid = :vid
  ", array(':separator' => TAXONOMY_MPATH_SEPARATOR, ':vid' => $vid, ':depth' => $depth, ':join' => '-'));
  $rows = $result->rowCount();
  $terms += $rows;

  if ($rows == 0) {
    $context['results'][] = t('Processed tree with %terms terms and depth %depth - took %time seconds', array(
                                '%terms' => $terms, 
                                '%depth' => $depth, 
                                '%time' => sprintf("%.08f", microtime(TRUE) - $context['sandbox']['time'])
                            ));
    $context['finished'] = 1;
    return;
  }
  $context['message'] = t('Processed tree depth %depth', array('%depth' => $depth)); 
  $depth++;
  $context['finished'] = 1 - (1 / $depth);
}

